import 'package:flutter/material.dart';

//main color
const PRIMARY_COLOR = Color(0xFF22A45D);
//font color
const BODY_TEXT_COLOR = Color(0xFF868686);
//TextField background color
const INPUT_BG_COLOR = Color(0xFFFBFBFB);
//TextField border color
const INPUT_BORDER_COLOR = Color(0xFFF3F2F2);
